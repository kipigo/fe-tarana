import React from "react";
import { connect } from "react-redux";
import { Row, Col } from 'antd';
import { Layout, Button } from "antd";
import { history } from "../store/store";
import ReactCountdownClock from "react-countdown-clock";
import AuthActions from "../actions/AuthActions";
const { Header } = Layout;

class Main extends React.Component {

    logout(){
        this.props.requestLogout()
    }

  render() {
    const { children, requestLogout } = this.props;
    const user = localStorage.getItem("user");
    return (
      <Layout>
        <div></div>
        <Header style={{ backgroundColor: 'white' , boxShadow:`0px 10px 24px -4px rgba(0,0,0,0.27)` , zIndex:2 }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: 15
            }}
          >
            <div style={{ display: "flex" }}>
              <Button type="link"  onClick={() => history.push("/desk")}>Home</Button>
              <Button type="link"  onClick={() => history.push("/dest/mybookings")}>My Bookings</Button>
              <Button type="link"  onClick={() => history.push("/desk")}>Created Trips</Button>
            </div>
            <div style={{ display: "flex" }}>
              {/* <Button type="link"  onClick={() => requestLogout()}>Account</Button> */}
              <Button type="link"  onClick={() =>this.logout()}>Logout</Button>
            </div>
          </div>
        </Header>
        <div style={{backgroundColor: 'white', position: 'absolute', top: 90, left: 0, right: 0, bottom: 0}}>
  
          <div style={{ display: "flex", justifyContent: "center", }}>
            {user ? (
              children
            ) : (
              <div style={{ justifyContent: "center", marginTop: '30vh' }}>
                    <div style={{ display: "flex", justifyContent: "center", margin: '2vh' }}>
                  <ReactCountdownClock
                    seconds={5}
                    onComplete={()=> history.push('/login')}
                    alpha={0.9}
                    size={50}
                  />
                </div>
                <div style={{textAlign: 'center'}} >
                <div style={{fontWeight: 'bold', fontSize: 12}}>You cannot view this item</div>
                <div style={{ fontSize: 10}}>You will be redirected to login page</div>
                </div>
              </div>
            )}
          </div>
        </div>
        
      </Layout>
    );
  }
}

export default connect(
  state => (null),
  dispatch => ({
    login(value){
        dispatch(AuthActions.RequestAuth(value))
    },
    requestLogout(value){
        dispatch(AuthActions.RequestLogut(value))
    },
  })
)(Main);
