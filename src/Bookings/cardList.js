import React from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import { Layout, Card, Icon, Avatar, Button } from "antd";
import { history } from "../store/store";
import ReactCountdownClock from "react-countdown-clock";
import moment from "moment";
import destinationActions from "./destinationActions";
const { Header } = Layout;
const { Meta } = Card;

class CardList extends React.Component {
  handleView(item) {
    this.props.selectDest(item);
    history.push("/dest/view");
  }
  render() {
    const { header, description, list } = this.props;
    return (
      <Layout style={{ margin: 20 }}>
        <div style={{ padding: 20 }}>
          <div style={{ fontSize: 20, fontWeight: 500 , color: 'darkorange'}}>{header}</div>
          <div style={{ fontSize: 15, fontWeight: 400 , color: 'darkgrey'}}>{description}</div>
          <div style={{ display: "flex", marginTop: 20 }}>
            {list
              ? list.map(iti => {
                  return (
                    <Card
                      style={{ width: 300, marginRight: 10 , }}
                      cover={
                        <img
                          style={{ height: 200, width: 300 }}
                          alt={iti.name}
                          src={iti.photo}
                        />
                      }
                      actions={[<div
                   
                      onClick={()=>this.handleView(iti)}
                      >CHECK THIS TRIP</div>]}
                    >
                      <div
                        style={{
                          marginBottom: "2vh",
                          letterSpacing: 2,
                          textTransform: "uppercase",
                          fontSize: 12
                        }}
                      >
                        {iti.joined} / {iti.max} Booked
                      </div>

                      <Meta
                        title={<div style={{color: 'darkorange'}}>{iti.name}</div>}
                        description={
                          <div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between"
                              }}
                            >
                            <div style={{color: iti.status == 'OPEN'? '#7fb9b9': 'grey', fontWeight: 800}}>{iti.status}</div>
                              <div>{iti.price}</div>
                            </div>
                            <div
                              style={{
                                marginBottom: "2vh",
                                letterSpacing: 2,
                                textTransform: "uppercase",
                                fontSize: 12
                              }}
                            >
                              {moment(iti.dates[0]).format("DD MMM")} -{" "}
                              {moment(iti.dates[1]).format("DD MMM")}
                            </div>
                          </div>
                        }
                      />
                    </Card>
                  );
                })
              : []}
          </div>
        </div>
      </Layout>
    );
  }
}

export default connect(
  state => null,
  dispatch => ({
    selectDest(item){
      dispatch(destinationActions.selectDest(item))
    }
  })
)(CardList);
