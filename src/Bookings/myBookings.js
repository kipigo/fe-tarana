import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Row, Col } from "antd";
import { Layout, Card, Icon, Avatar, Button, List, Skeleton } from "antd";
import CardList from "./cardList";
import BookingActions from "./bookingActions";
import { history } from "../store/store";
import ReactCountdownClock from "react-countdown-clock";
import DestinationActions from "./destinationActions";
const { Header } = Layout;
const { Meta } = Card;

class MyBookings extends React.Component {
  state = {
    initLoading: true,
    loading: false,
    data: [],
    list: []
  };

  componentDidMount() {
    const { getBooked, booked } = this.props;
  
    getBooked();
  }

  render() {
    const {  booked } = this.props;
    const { initLoading, loading, list } = this.state;
    const loadMore =
      !initLoading && !loading ? (
        <div
          style={{
            textAlign: "center",
            marginTop: 12,
            height: 32,
            lineHeight: "32px"
          }}
        >
          <Button onClick={this.onLoadMore}>loading more</Button>
        </div>
      ) : null;
    return (

      <Layout style={{ margin: 20 }}>
    
        <List
          dataSource={booked}
          renderItem={item => (
        
            <List.Item
            
     
            key={item.id}>
              <List.Item.Meta
                avatar={
                  <Avatar 
                  style={{marginLeft:20}}
                  src={item['travelDetails'].photo} />
                }
                title={<a href="https://ant.design">{item['travelDetails'].name}</a>}
                description={moment(item.travelDate[0]).format('DD MMM YY') - moment(item.travelDate[1]).format('DD MMM YY')}
              />
                <div style={{padding:10, 
                  marginRight: 40, fontSize: 12, 
                  letterSpacing: 2}}> Status : {item.status}</div>
              {/* <div style={{padding:10}}> Transaction ID : {moment(item.travelDate[0]).format('DD MMM YY') - moment(item.travelDate[1]).format('DD MMM YY')}</div> */}
              <div style={{padding:10, marginRight: 40, fontSize: 12, letterSpacing: 2}}> Transaction ID : 4349u3284329489238fdjf</div>
              <div style={{padding:10, marginRight: 40, fontSize: 12, letterSpacing: 2}}> Date Booked: {moment(item.dateBooked).format('DD MMM YY')}</div>
            </List.Item>
          )}
        >
          {/* {this.state.loading && this.state.hasMore && (
            <div className="demo-loading-container">
              <Spin />
            </div>
          )} */}
        </List>
      </Layout>
    );
  }
}

export default connect(
  state => ({
    booked: state.booking.bookings
  }),
  dispatch => ({
    getBooked() {
      dispatch(BookingActions.getBooked());
    },
    joinBooking(value){
      dispatch(DestinationActions.joinTrip(value))
    }
  })
)(MyBookings);
