import React from "react";
import { connect } from "react-redux";
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,Upload,
  AutoComplete,
  message,
  DatePicker
} from "antd";
import { Layout, Card, Avatar } from "antd";
import CardList from "./cardList";
import DestionationActions from "./destinationActions";
import { history } from "../store/store";
import ReactCountdownClock from "react-countdown-clock";
const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
}

class CreateBooking extends React.Component {
  state = {
    loading: false
  };

  componentDidMount() {
    const { getDestination } = this.props;
    getDestination();
  }

  handleChange = info => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false
        })
      );
    }
  };

  render() {
    const { imageUrl } = this.state;
    const user = localStorage.getItem("user");
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div style={{height: '40vh', width: '40vh'}} className="ant-upload-text">Upload Photo Here</div>
      </div>
    );
    return (
        <div style={{width: '100%'}}>
        <div style={{display: 'flex', justifyContent:'flex-end', margin:20}}>
        <Button
        onClick={()=>message.success('Your trip has been added successfully added.')}
        >Create This Trip</Button>
        </div>

      <Layout style={{ margin: 20 }}>
       
        <Form
          onSubmit={() => message.success("Your created trip has beens saved")}
        >
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Name : <Input size="large" placeholder="large size" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Location : <Input size="large" placeholder="Location" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
          
            Travel Dates:{" "}
            <DatePicker.RangePicker size="large" placeholder="Event Name" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
          
          Closing Date:{" "}
          <DatePicker size="large" placeholder="Last Day of Booking" />
        </div>
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Price : <Input size="large" placeholder="Price" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Max Travellers : <Input size="large" placeholder="Maximum Number of Travelers" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Inclusions: <Input.TextArea size="large" placeholder="Trip Inclusions" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
            {" "}
            Itinerary: <Input.TextArea size="large" placeholder="Itinerary" />
          </div>
          <div style={{ margin: 10, padding: 10 }}>
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img src={imageUrl} alt="avatar" style={{ width: "auto" , height: '40vh'}} />
              ) : (
                uploadButton
              )}
            </Upload>
          </div>
          {/* <div><Input size="large" placeholder="large size" /></div>
         <div><Input size="large" placeholder="large size" /></div>
         <div><Input size="large" placeholder="large size" /></div>
         <div><Input size="large" placeholder="large size" /></div> */}
        </Form>
      </Layout>
      </div>
    );
  }
}

export default connect(
  state => ({
    destinations: state.destination.destinations
  }),
  dispatch => ({
    getDestination() {
      dispatch(DestionationActions.getDestinations());
    },
    clear() {
      dispatch(DestionationActions.clear());
    }
  })
)(CreateBooking);
