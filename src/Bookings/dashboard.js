import React from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import { Layout, Card, Icon, Avatar, Button } from "antd";
import CardList from "./cardList";
import DestionationActions from "./destinationActions";
import { history } from "../store/store";
import ReactCountdownClock from "react-countdown-clock";
const { Header } = Layout;
const { Meta } = Card;

class Dashboard extends React.Component {
  componentDidMount() {
    const { getDestination } = this.props;
    getDestination();
  }

  render() {
    const { destinations } = this.props;
    const user = localStorage.getItem("user");
    return (
      <Layout style={{ margin: 20 }}>
        {destinations
          ? destinations.map(item => {
              return (
                <CardList
                  list={item.list}
                  header={item.title}
                  description={item.description}
                />
              );
            })
          : []}
      </Layout>
    );
  }
}

export default connect(
  state => ({
    destinations: state.destination.destinations
  }),
  dispatch => ({
    getDestination() {
      dispatch(DestionationActions.getDestinations());
    },
    clear() {
      dispatch(DestionationActions.clear());
    }
  })
)(Dashboard);
