import BookingServices from "../services/BookingServices";
import { notification } from "antd";
import { history } from "../store/store";

export const BookingActionTypes = {
  REQUEST: "@@REQUEST",
  SELECT: "@@SELECT",
  SUCCESS: "@@SUCCESS",
  ERROR: "@@ERROR",
  CLEAR: "@@CLEAR"
};

export default {


  getBooked() {
    return async dispatch => {
      try {
        dispatch({
          type: BookingActionTypes.REQUEST
        });
        let destinations = await BookingServices.getBookedTrips()
        dispatch({
          type: BookingActionTypes.SUCCESS,
          payload: destinations
        });
      } catch (e) {
        dispatch({
          type: BookingActionTypes.ERROR,
          payload: e
        });
      }
    };
  },
  selectDest(value) {
    return dispatch => {
      try {
        dispatch({
          type: BookingActionTypes.REQUEST
        });
        dispatch({
          type: BookingActionTypes.SELECT,
          payload: value
        });
      } catch (e) {
        dispatch({
          type: BookingActionTypes.ERROR,
          payload: e
        });
      }
    };
  },

};
