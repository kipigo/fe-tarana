import React from "react";
import { connect } from "react-redux";
import barcode from "../assets/53.png";
import bux from "../assets/logo.png";
import {
  Layout,
  Card,
  Rate,
  Steps,
  Avatar,
  Button,
  Modal,
  Radio,

  Descriptions,
  notification
} from "antd";
import { history } from "../store/store";
import destinationActions from "./destinationActions";

const { Step } = Steps;

class DestView extends React.Component {
  state = {
    open: false,
    current: 0,
    choice: ""
  };

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  onChange = e => {
    this.setState({
      choice: e.target.value
    });
  };

  onSubmit() {
   
    notification.success({
        message :"Your booking will be confirmed upon completion of payment. Thank you for choosing ComeWithUs"
    }
 
    );
    // this.props.joinTrip('value')
    this.setState({
        open: false
    })
  }

  renderRadio(props) {
    return (
      <div>
        <Radio.Group value={this.state.choice} onChange={this.onChange}>
          <div style={{ margin: 10 }}>
            <Radio value={"UBP"}>UnionBank App</Radio>
          </div>
          <div style={{ margin: 10 }}>
            <Radio value={"BUX"}>Bux</Radio>
          </div>
        </Radio.Group>
      </div>
    );
  }

  renderBarcode(props) {
    return (
      <div>
        <div>
          Please scan the barcode below and present this to any BUX retailers.
          <div>
            <img style={{ width: 400, height: 200 }} src={barcode} />
          </div>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <div>
              Generated Using{" "}
              <img style={{ width: "auto", height: 10 }} src={bux} />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const steps = [
      {
        title: "Payment Method",
        content: this.renderRadio()
      },
      {
        title: "Payment Details",
        content: this.renderBarcode()
      }
    ];
    const { destination } = this.props;
    const { current } = this.state;
    return (
      <div style={{ margin: 20, width: "100%" }}>
        <Modal
          onCancel={() => this.setState({ open: false })}
          okText={current == steps.length - 1 ? "OK" : "Next"}
          cancelText={current > 0 ? "Previous" : "Cancel"}
          onOk={
            current == steps.length - 1
              ? () => this.onSubmit()
              : () => this.next()
          }
          visible={this.state.open}
        >
          <div style={{ padding: 20 }}>
            <Steps current={current}>
              {steps.map(item => (
                <Step key={item.title} title={item.title} />
              ))}
            </Steps>
            <div style={{ padding: 20 }}>{steps[current].content}</div>
          </div>
        </Modal>
        <div
          style={{ display: "flex", justifyContent: "flex-end", padding: 20 }}
        >
              <Button style={{marginRight:20}} onClick={() => history.go(-1)}>
           BACK
          </Button>
          <Button type="primary" onClick={() => this.setState({ open: true })}>
            JOIN THIS TRIP
          </Button>
        </div>
        <div style={{ padding: 20 }}>
          <Descriptions bordered title={"Trip Details"} border size={"large"}>
            <Descriptions.Item label="Name">
              {destination.name}
            </Descriptions.Item>
            <Descriptions.Item label="Created By">
              {destination.name}
            </Descriptions.Item>
            <Descriptions.Item label="Rating">
              <Rate allowHalf defaultValue={2.5} />
            </Descriptions.Item>
            <Descriptions.Item label="PRICE">
              {destination.price}
            </Descriptions.Item>
            <Descriptions.Item label="LOCATION">
              {destination.name}
            </Descriptions.Item>
            <Descriptions.Item label="JOINERS">
              {destination.joined}
            </Descriptions.Item>
            <Descriptions.Item label="CAPACITY">
              {destination.max}
            </Descriptions.Item>
            <Descriptions.Item label="Date From">
              {destination.dates ? destination.dates[0] : ""}
            </Descriptions.Item>
            <Descriptions.Item label="Date To">
              {destination.datesÍ ? destination.dates[1] : ""}
            </Descriptions.Item>
          </Descriptions>
        </div>
        <div style={{ display: "flex" }}>
          <div style={{ flex: 6 }}>
            <div style={{ display: "flex", padding: 20 }}>
              PEOPLE JOINED THIS TRIP
            </div>
            <div style={{ display: "flex", padding: 20 }}>
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              <div style={{ marginLeft: 20, flex: 1 }}>
                <div>Name, Name</div>
                <div>Antipolo City</div>
              </div>
              <div style={{ marginLeft: 20 }}>
                <Rate allowHalf defaultValue={2.5} />
              </div>
            </div>
            <div style={{ display: "flex", padding: 20 }}>
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              <div style={{ marginLeft: 20, flex: 1 }}>
                <div>Name, Name</div>
                <div>Antipolo City</div>
              </div>
              <div style={{ marginLeft: 20 }}>
                <Rate allowHalf defaultValue={2.5} />
              </div>
            </div>

            <div style={{ display: "flex", padding: 20 }}>
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              <div style={{ marginLeft: 20, flex: 1 }}>
                <div>Name, Name</div>
                <div>Antipolo City</div>
              </div>
              <div style={{ marginLeft: 20 }}>
                <Rate allowHalf defaultValue={2.5} />
              </div>
            </div>

            <div style={{ display: "flex", padding: 20 }}>
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              <div style={{ marginLeft: 20, flex: 1 }}>
                <div>Name, Name</div>
                <div>Antipolo City</div>
              </div>
              <div style={{ marginLeft: 20 }}>
                <Rate allowHalf defaultValue={2.5} />
              </div>
            </div>
          </div>
          <div style={{ flex: 6 }}>
            <div style={{ display: "flex", padding: 20 }}>ITINERARY</div>
            <div style={{ padding: 20 }}>
              <div style={{ fontSize: 12, fontWeight: 800 }}>Day 1</div>
              <div style={{ fontSize: 12, fontWeight: 300, letterSpacing: 2 }}>
                8:00-10:00
              </div>
              <div style={{ marginBottom: 10 }}>
                Explore Bucharest, the capital of Romania. In the evening enjoy
                a concert by George Enescu Philharmonic Orchestra or travel to
                Valea Calugareasca, one of the top Romanian wine regions. After
                wine tasting and dinner return to Bucharest.
              </div>
              <div style={{ fontSize: 12, fontWeight: 300, letterSpacing: 2 }}>
                10:00-12:00
              </div>
              <div style={{ marginBottom: 10 }}>
                Explore Bucharest, the capital of Romania. In the evening enjoy
                a concert by George Enescu Philharmonic Orchestra or travel to
                Valea Calugareasca, one of the top Romanian wine regions. After
                wine tasting and dinner return to Bucharest.
              </div>
              <div style={{ fontSize: 12, fontWeight: 300, letterSpacing: 2 }}>
                12:00-14:00
              </div>
              <div style={{ marginBottom: 10 }}>
                Explore Bucharest, the capital of Romania. In the evening enjoy
                a concert by George Enescu Philharmonic Orchestra or travel to
                Valea Calugareasca, one of the top Romanian wine regions. After
                wine tasting and dinner return to Bucharest.
              </div>
              <div style={{ fontSize: 12, fontWeight: 300, letterSpacing: 2 }}>
                14:00-16:00
              </div>
              <div style={{ marginBottom: 10 }}>
                Explore Bucharest, the capital of Romania. In the evening enjoy
                a concert by George Enescu Philharmonic Orchestra or travel to
                Valea Calugareasca, one of the top Romanian wine regions. After
                wine tasting and dinner return to Bucharest.
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    destination: state.destination.selected
  }),
  dispatch => ({
    joinTrip(value){
dispatch(destinationActions.joinTrip(value))
    }
  })
)(DestView);
