import DestinationServices from "../services/DestionationService";
import { notification } from "antd";
import { history } from "../store/store";

export const DestinationActionTypes = {
  REQUEST: "@@REQUEST_DEST",
  SELECT: "@@SELECT_DEST",
  SUCCESS: "@@SUCCESS_DEST",
  ERROR: "@@ERROR_DEST",
  CLEAR: "@@CLEAR_DEST"
};

export default {
  getDestinations() {
    return async dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.REQUEST
        });
        let destinations = await DestinationServices.getDestinations();
        dispatch({
          type: DestinationActionTypes.SUCCESS,
          payload: destinations
        });
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  },

  getBooked() {
    return async dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.REQUEST
        });
        let destinations = await DestinationServices.getBookedTrips();
        dispatch({
          type: DestinationActionTypes.SUCCESS,
          payload: destinations
        });
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  },
  selectDest(value) {
    return dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.REQUEST
        });
        dispatch({
          type: DestinationActionTypes.SELECT,
          payload: value
        });
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  },

  joinTrip(value) {
    return async dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.REQUEST
        });
        let destinations = await DestinationServices.joinTrip(value);
        history.push("/dest/mybookings");
        return destinations;
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  },

  addTrip(value) {
    return async dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.REQUEST
        });
        let destinations = await DestinationServices.addTrip(value);
        return destinations;
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  },

  clear() {
    return async dispatch => {
      try {
        dispatch({
          type: DestinationActionTypes.CLEAR
        });
      } catch (e) {
        dispatch({
          type: DestinationActionTypes.ERROR,
          payload: e
        });
      }
    };
  }
};
