import React, { Suspense, lazy } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store, { history} from './store/store'
import Main from './Layout/layout'
import './App.css';
import "antd/dist/antd.css";

function App() {
  return (
 
    <Provider store={store()}>
      <Router history={history}>
        <Switch>
          <Route
            exact
            path={"/login"}
            component={WaitingComponent(
              lazy(() => import("./Login/login"))
            )}
          />
        
          <Main>
            <Switch>
              <Route exact path={"/desk"}
                component={WaitingComponent(
                  lazy(() => import("./Bookings/dashboard"))
                )}
                />
               <Route exact path={"/dest/view"}
                component={WaitingComponent(
                  lazy(() => import("./Bookings/destinationView"))
                )}
                />
                 <Route exact path={"/dest/mybookings"}
                component={WaitingComponent(
                  lazy(() => import("./Bookings/myBookings"))
                )}
                />
                  <Route exact path={"/dest/create"}
                component={WaitingComponent(
                  lazy(() => import("./Bookings/createBooking"))
                )}
                />
            </Switch>
          </Main>
     
        </Switch>
      </Router>
      </Provider>
  
  );
}

function WaitingComponent(Component) {
  return props => (
    <Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}


export default App;

