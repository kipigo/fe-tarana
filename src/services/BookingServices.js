// import {authHeader} from './header'
import axios from "axios";
const destinations = [
    {
        title:'Travel Now',
        description:'Got Nothing To Do? Travel Nearby',
        list: [
            {
                name: 'Pasig Half Day Tour',
                price: 2000,
                dates: [new Date().toDateString(),new Date().toDateString()],
                joined: 10,
                max: 10,
                status: 'SOLD OUT',
                photo: `https://upload.wikimedia.org/wikipedia/commons/0/04/Ortigas_Center_Manila.JPG`
            },
            {
                name: 'Pasig Night Life',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRfejX2Iep2-XGBnJmelDC3qxGQeO5bYFvMD4u9zzH1nyiNSz4`
            },
            {
                name: 'Antipolo',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://businessmirror.com.ph/wp-content/uploads/2018/09/tourism01-092318.jpg`
               },
            {
                name: 'Makati',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://www.diamzon.com/wp-content/uploads/2016/11/makaticbd-990x530.jpg`
            },
        ]
    },
    {
        title:'Book Your Dream Destination',
        description:'Have Fun and Travel to Your Dream Destination with other travelers',
        list: [
            {
                name: 'Laguna 2-Day Tour',
                price: 2000,
                dates: [new Date().toDateString(),new Date().toDateString()],
                joined: 10,
                max: 10,
                status: 'SOLD OUT',
                photo: `https://upload.wikimedia.org/wikipedia/commons/0/04/Ortigas_Center_Manila.JPG`
            },
            {
                name: 'Ilocos 3-Day Tour',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRfejX2Iep2-XGBnJmelDC3qxGQeO5bYFvMD4u9zzH1nyiNSz4`
            },
        ]
    },
    {
        title:`Don't Know Where To Go?`,
        description:`Can't decide where to go? Try these popular destinations`,
        list: [
            
        ]
    }
]


const bookedServices = [
    {
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    },
    {
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    },{
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    }

]


// const header = authHeader();
class BookingService {


  static async getBookedTrips() {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return bookedServices;
  }

  static async addTrip(value) {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return 'success';
  }


  static async joinTrip(value) {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return 'success';
  }


  static async RequestLogout(username, password) {
    localStorage.removeItem("user");
    return null;
  }
}

export default BookingService;
