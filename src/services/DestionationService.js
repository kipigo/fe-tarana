// import {authHeader} from './header'
import axios from "axios";
const destinations = [
    {
        title:'Travel Now',
        description:'Got Nothing To Do? Travel Nearby',
        list: [
            {
                name: 'Pasig Half Day Tour',
                price: 2000,
                dates: [new Date().toDateString(),new Date().toDateString()],
                joined: 10,
                max: 10,
                status: 'SOLD OUT',
                photo: `https://upload.wikimedia.org/wikipedia/commons/0/04/Ortigas_Center_Manila.JPG`
            },
            {
                name: 'Pasig Night Life',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRfejX2Iep2-XGBnJmelDC3qxGQeO5bYFvMD4u9zzH1nyiNSz4`
            },
            {
                name: 'Antipolo',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://businessmirror.com.ph/wp-content/uploads/2018/09/tourism01-092318.jpg`
               },
            {
                name: 'Makati',
                price: 2000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://www.diamzon.com/wp-content/uploads/2016/11/makaticbd-990x530.jpg`
            },
        ]
    },
    {
        title:'Book Your Dream Destination',
        description:'Have Fun and Travel to Your Dream Destination with other travelers',
        list: [
            {
                name: 'Laguna 2-Day Tour',
                price: 2000,
                dates: [new Date().toDateString(),new Date().toDateString()],
                joined: 8,
                max: 10,
                status: 'OPEN',
                photo: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZzG79-8vnRvjuluSMA1gGtm4OBXZEhjSdSjel3zZoUjjnUzEGsw`
            },
            {
                name: 'Ilocos 4-Day Tour',
                price: 4000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://upload.wikimedia.org/wikipedia/commons/3/35/Bangui_Windmills%2C_East_View%2C_Ilocos_Norte%2C_Philippines_-_panoramio.jpg`
            },
            {
                name: 'Balacbac Palawan 4-Day Tour',
                price: 5000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://i.pinimg.com/736x/40/51/e7/4051e7f3c669d5ac82e01183469d21b2.jpg`
            },
        ]
    },
    {
        title:`Don't Know Where To Go?`,
        description:`Can't decide where to go? Try these popular destinations`,
        list: [
            {
                name: 'Siargao',
                price: 5000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://s27922.pcdn.co/wp-content/uploads/2018/03/siargao-6.jpg.optimal.jpg`
            },
            {
                name: 'Dumaguete',
                price: 5000,
                dates: [new Date(), new Date()],
                joined: 10,
                max: 15,
                status: 'OPEN',
                photo: `https://travelosyo.com/wp-content/uploads/2018/01/Manjuyod-sandbar-2-900x675.jpg`
            },
        ]
    }
]


const bookedServices = [
    {
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    },
    {
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    },{
        dateBooked: new Date(),
        travelDate:[new Date(), new Date()],
        travelDetails : destinations[0].list[0],
        status: 'AWAITING PAYMENT',
        paymentMode: 'BUX',
        transactionID: '773827382jdsdy764jweywdaF'
    }

]


// const header = authHeader();
class DestinationService {
  static async getDestinations() {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return destinations;
  }

  static async getBookedTrips() {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return bookedServices;
  }

  static async addTrip(value) {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return 'success';
  }


  static async joinTrip(value) {
    // let fd = new FormData();
    // fd.append("usr", "kerwin@ubx.ph");
    // fd.append("pwd", "445474KK");
    // let user = await axios.post(
    //   `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/method/login`
    // );

    return 'success';
  }


  static async RequestLogout(username, password) {
    localStorage.removeItem("user");
    return null;
  }
}

export default DestinationService;
