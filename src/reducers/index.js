import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import Destination from "./destination";
import Booking from "./booking";
// import DeskReducer from './DeskReducer'

export default combineReducers({
  router: routerReducer,
  destination: Destination,
  booking: Booking
  //   auth: AuthReducer,
  //   desk: DeskReducer
});
