import { DestinationActionTypes } from "../Bookings/destinationActions";

const initialState = {
  destinations: [],
  selected: [],
  loading: false,
  error: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DestinationActionTypes.SELECT:
      return {
        selected: action.payload
      };
    case DestinationActionTypes.REQUEST:
      return {
        loading: true
      };
    case DestinationActionTypes.SUCCESS:
      return {
        destinations: action.payload,
        loading: false
      };
    case DestinationActionTypes.ERROR:
      return {
        error: action.payload,
        loading: false
      };
    case DestinationActionTypes.CLEAR:
      return {
        initialState
      };
    default:
      return state;
  }
};
