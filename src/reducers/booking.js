import { BookingActionTypes } from "../Bookings/bookingActions";

const initialState = {
  bookings: [],
  selected: [],
  loading: false,
  error: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case BookingActionTypes.SELECT:
      return {
        selected: action.payload
      };
    case BookingActionTypes.REQUEST:
      return {
        loading: true
      };
    case BookingActionTypes.SUCCESS:
      return {
        bookings: action.payload,
        loading: false
      };
    case BookingActionTypes.ERROR:
      return {
        error: action.payload,
        loading: false
      };
    case BookingActionTypes.CLEAR:
      return {
        initialState
      };
    default:
      return state;
  }
};
