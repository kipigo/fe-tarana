import React from "react";
import { Input, Button, Carousel } from "antd";
import { Row, Col } from "antd";
import { connect } from "react-redux";
import img1 from "../assets/photo1.jpg";
import img2 from "../assets/photo2.jpg";
import FacebookLogin from "react-facebook-login";
// import TiSocialFacebookCircular from "react-icons/lib/ti/social-facebook-circular";
import AuthActions from "../actions/AuthActions";



const style = {
  input: {
    fontSize: 10,
    marginBottom: "2vh"
  },
  flexCenter: {
    display: "flex",
    justifyContent: "center",
    margin: "1vh"
  }
};

class Login extends React.Component {
  state = {
    password: null,
    username: null
  };

  renderImages = () => {
    let fields = [img1, img2];
    return fields;
  };

  render() {
    const { history, authRequest } = this.props;
    const { username, password } = this.state;
    return (
      <div>
        <div
          style={{
             backgroundImage: `url(${img1})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            zIndex: -1
          }}
        >
          <Carousel
         
            autoplay
          >
            <div
              style={{
                backgroundImage: `url(${img1})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                position: "absolute",
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: -1
              }}
            />
            <div
              style={{
                backgroundImage: ` -webkit-linear-gradient(top, 
                    rgba(0,0,0,0.9) 0%, 
                    rgba(0,0,0,0) 20%,
                    rgba(0,0,0,0) 80%,
                    rgba(0,0,0,0.9) 100%
                  ),
                  -webkit-linear-gradient(left, 
                    rgba(0,0,0,0.9) 0%, 
                    rgba(0,0,0,0) 20%,
                    rgba(0,0,0,0) 80%,
                    rgba(0,0,0,0.9) 100%
                  ), url(${img2})`,
               
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                position: "absolute",
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: -1
              }}
            />
          </Carousel>
        </div>
        <div
          style={{
            backgroundColor: "#ff3d33",
            width: "100%",
            height: "8vh",
            fontSize: 40,
            marginLeft:60,
            paddingLeft:20,
            fontWeight:800,
            opacity: 0.9
          }}
        >
       <span style={{color: 'white'}}> TARA</span> 
         <span style={{color: '#ffa500'}}>NA</span>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            marginRight: "20vh",
            marginTop: "5vh"
          }}
        >
          <div>
            <div style={{  color: "white" , marginBottom: '5vh'}}>
              <div style={{fontWeight: 800, fontSize: 60, textShadow:'2px 2px  10px #000'}}>Join Other Travelers</div>
              <div style={{fontWeight: 500, fontSize: 40, textShadow:'2px 2px  8px #000'}}>Create New Friends</div>
              <div style={{fontWeight: 500, fontSize: 40, textShadow:'2px 2px  8px #000'}}>Book Your Dream </div>
              <div style={{fontWeight: 500, fontSize: 40, textShadow:'2px 2px  8px #000'}}>Destinations for Less.</div>
            </div>

            <div style={{ backgroundColor: "white", padding: 20 , borderRadius: '10px', opacity: .8}}>
              <Input
                size="large"
                onChange={value =>
                  this.setState({ username: value.target.value })
                }
                style={style.input}
              />
              <Input.Password
                size="large"
                onChange={value =>
                  this.setState({ password: value.target.value })
                }
                style={style.input}
              />
              <div style={style.flexCenter}>
                <Button
                  onClick={() => authRequest(username, password)}
                  type="primary"
                >
                  Submit
                </Button>
                <Button 

                
                type="link">Sign Up</Button>
              </div>
              <div style={{ ...style.flexCenter, marginTop: "5vh" }}>
                <FacebookLogin
                  appId="1088597931155576"
                  autoLoad={true}
                  fields="name,email,picture"
                  // callback={responseFacebook}
                  cssClass="my-facebook-button-class"
                  icon="fa-facebook"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    authRequest(username, password) {
      dispatch(AuthActions.RequestAuth(username, password));
    },
    
  })
)(Login);


